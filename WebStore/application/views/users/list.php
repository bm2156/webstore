<div class="my-3 d-flex justify-content-between">
    <h2><?= $title ?></h2>
    <a href="<?php echo base_url(); ?>users/create" class="btn btn-success">Create</a>
</div>

<div class="list-group">
    <?php foreach ($users as $user) { ?>
        <a href="<?php echo base_url(); ?>users/<?= $user["idUporabnik"] ?>" class="
           list-group-item 
           list-group-item-action 
           align-items-start <?php
           if ($user["aktiven"] == 1) {
               echo "list-group-item-success";
           } else {
               echo "list-group-item-danger";
           }
           ?>">
            <div>
                <h5><?= $user["ime"] . " " . $user["priimek"] ?></h5>
            </div>
        </a>
    <?php } ?>
</div>

