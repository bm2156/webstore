<h2 class="my-3"><?= $title ?></h2>

<hr>

<h4 class="mb-3">Personal Details</h4>

<?php
if (form_error('password') == NULL) {
    echo validation_errors();
}
?>

<?php echo form_open('profile/update_details') ?>
<div style="width: 30%">
    <div class="input-group mb-3">
        <input required type="text" class="form-control" name="firstname" placeholder="First name" aria-label="firstname" aria-describedby="basic-addon1" value="<?= $user["ime"] ?>">
    </div>
    <div class="input-group mb-3">
        <input required type="text" class="form-control" name="lastname" placeholder="Last name" aria-label="lastname" aria-describedby="basic-addon1" value="<?= $user["priimek"] ?>">
    </div>
<?php if ($user["idVloga"] == 3) { ?>
        <div class="input-group mb-3">
            <input required type="text" class="form-control" name="address" placeholder="Address" aria-label="address" aria-describedby="basic-addon1" value="<?= $user["naslov"] ?>">
        </div>
        <div class="input-group mb-3">
            <input required type="text" class="form-control" name="phoneNumber" placeholder="Phone number" aria-label="phoneNumber" aria-describedby="basic-addon1" value="<?= $user["telefon"] ?>">
        </div>
<?php } ?>
    <div class="input-group mb-3">
        <input required type="email" class="form-control" name="email" placeholder="Email" aria-label="email" aria-describedby="basic-addon1" value="<?= $user["email"] ?>">
    </div>
    <div>
        <input type="submit" class="btn btn-success" value="Update details">
    </div>
</div>
</form>

<hr>

<h4 class="mb-3">Change Password</h4>

<?php echo form_error('password'); ?>

<?php echo form_open('profile/change_password') ?>
<div style="width: 30%">
    <div class="input-group mb-3">
        <input type="password" class="form-control" name="password" placeholder="Password" aria-label="password" aria-describedby="basic-addon1">
    </div>
    <div class="input-group mb-3">
        <input type="password" class="form-control" name="changedPassword" placeholder="Updated password" aria-label="password" aria-describedby="basic-addon1">
    </div>
    <div>
        <input type="submit" class="btn btn-success" value="Update password">
    </div>
</div>
</form>

