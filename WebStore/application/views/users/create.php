<h2 class="my-3"><?= $title ?></h2>

<?php echo validation_errors() ?>

<?php echo form_open('auth/register_process') ?>
<div style="width: 30%">
    <div class="input-group mb-3">
        <input required type="text" class="form-control" name="firstname" placeholder="First name" aria-label="firstname" aria-describedby="basic-addon1">
    </div>
    <div class="input-group mb-3">
        <input required type="text" class="form-control" name="lastname" placeholder="Last name" aria-label="lastname" aria-describedby="basic-addon1">
    </div>
    <?php if ($user["idVloga"] == 2) { ?>
        <div class="input-group mb-3">
            <input required type="text" class="form-control" name="address" placeholder="Address" aria-label="address" aria-describedby="basic-addon1">
        </div>
        <div class="input-group mb-3">
            <input required type="text" class="form-control" name="phoneNumber" placeholder="Phone number" aria-label="phoneNumber" aria-describedby="basic-addon1">
        </div>
    <?php } ?>
    <div class="input-group mb-3">
        <input required type="email" class="form-control" name="email" placeholder="Email" aria-label="email" aria-describedby="basic-addon1">
    </div>
    <div class="input-group mb-3">
        <input required type="password" class="form-control" name="password" placeholder="Password" aria-label="password" aria-describedby="basic-addon1">
    </div>
    <div>
        <a href="<?php echo base_url(); ?>users" class="btn btn-warning">Back</a>
        <input type="submit" class="btn btn-success mb-1" value="Create">
    </div>
</div>
</form>

