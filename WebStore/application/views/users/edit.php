<h2 class="my-3"><?= $title ?></h2>

<?php echo validation_errors() ?>

<?php echo form_open('users/edit_process') ?>
<div style="width: 30%">
    <input type="hidden" name="id" value="<?= $user["idUporabnik"] ?>">
    <div class="input-group mb-3">
        <input required type="text" class="form-control" name="firstname" placeholder="First name" aria-label="firstname" aria-describedby="basic-addon1" value="<?= $user["ime"] ?>">
    </div>
    <div class="input-group mb-3">
        <input required type="text" class="form-control" name="lastname" placeholder="Last name" aria-label="lastname" aria-describedby="basic-addon1" value="<?= $user["priimek"] ?>">
    </div>
    <?php if ($user["idVloga"] == 3) { ?>
        <div class="input-group mb-3">
            <input required type="text" class="form-control" name="address" placeholder="Address" aria-label="address" aria-describedby="basic-addon1" value="<?= $user["naslov"] ?>">
        </div>
        <div class="input-group mb-3">
            <input required type="text" class="form-control" name="phoneNumber" placeholder="Phone number" aria-label="phoneNumber" aria-describedby="basic-addon1" value="<?= $user["telefon"] ?>">
        </div>
    <?php } ?>
    <div class="input-group mb-3">
        <input required type="email" class="form-control" name="email" placeholder="Email" aria-label="email" aria-describedby="basic-addon1" value="<?= $user["email"] ?>">
    </div>
    <?php if (isset($user["geslo"])) { ?>
        <div class="input-group mb-3">
            <input required type="password" class="form-control" name="password" placeholder="Password" aria-label="password" aria-describedby="basic-addon1" value="<?= $user["geslo"] ?>">
        </div>
    <?php } ?>
    <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" name="active" id="customCheck1" <?= $user["aktiven"] == 1 ? 'checked="checked"' : NULL; ?>>
        <label class="custom-control-label" for="customCheck1">Aktiven</label>
    </div>
    <div class="mt-2">
        <a href="<?php echo base_url(); ?>users" class="btn btn-warning">Nazaj</a>
        <input type="submit" class="btn btn-success" value="Shrani">
    </div>
</div>
</form>

