<div class="my-3 d-flex justify-content-between">
    <h2><?= $title ?></h2>
</div>

<?php if (count($items) == 0) { ?>
    <h5 class="card-title">Your cart is empty</h5>
<?php } else { ?>

    <div class="list-group mb-3">
        <?php foreach ($items as $item) { ?>
            <div class="list-group-item d-flex align-items-center justify-content-between">
                <div>
                    <h4 class="card-title"><?= $item["product"]["naziv"] ?></h4>
                    <h6 class="card-title"><?= $item["product"]["cena"] * $item["amount"] ?> €</h6>
                </div>
                <div class="d-flex">
                    <?php echo form_open("users/add_to_cart/cart", array("class" => "mr-2")) ?>
                    <input type="hidden" value="<?= $item["product"]["idArtikel"] ?>" name="id">
                    <input type="number" 
                           min="0" 
                           value="<?php
                           echo $item["amount"];
                           ?>" name="amount" style="width: 50px;" class="mr-2">
                    <input type="submit" value="Update cart" class="btn btn-success">
                    </form>
                    <?php echo form_open("users/add_to_cart/cart") ?>
                    <input type="hidden" value="<?= $item["product"]["idArtikel"] ?>" name="id">
                    <input type="hidden" min="0" value="0" name="amount">
                    <input type="submit" value="Remove" class="btn btn-danger">
                    </form>
                </div>
            </div>
        <?php } ?>
        <div class="
             list-group-item 
             d-flex 
             align-items-center 
             justify-content-between
             list-group-item-secondary">
            <h4>Total: <?= $price_sum ?> €</h4>
        </div>
    </div>

<?php } ?>

<div class="d-flex">
    <div>
        <a href="<?php echo base_url(); ?>products" class="btn btn-warning mr-3">Browse store</a>
    </div>
    <?php echo form_open("cart/checkout") ?>
    <input type="submit" value="Submit order" class="btn btn-success <?php
           if (count($items) == 0) {
               echo "d-none";
           }
           ?>"
           >    </form>
</div>

