<div class="my-3">
    <h2><?= $title ?></h2>
</div>

<?php if (count($orders) == 0 && $user["idVloga"] == 2) { ?>
    <h5>There is no orders to process.</h5>
<?php } ?>

<?php if (count($orders) == 0 && $user["idVloga"] == 3) { ?>
    <h5>You have not ordered anything yet.</h5>
<?php } ?>

<div class="list-group">
    <?php foreach ($orders as $order) { ?>
        <a href="<?php echo base_url(); ?>orders/<?= $order["idNakup"] ?>" class="
           list-group-item 
           list-group-item-action 
           align-items-start">
            <div class="d-flex">
                <div>
                    <?php if ($user["idVloga"] == 2) { ?>
                        <h5>Customer: <?= $order["narocnik"]["ime"] . " " . $order["narocnik"]["priimek"] ?></h5>
                        <h5>Address: <?= $order["narocnik"]["naslov"] ?></h5>
                    <?php } ?>
                    <h5>Date: <?= date_format(date_create($order["cas_narocila"]), "d. M. Y") ?></h5>
                    <?php if ($user["idVloga"] == 2) { ?>
                        <h5>Time: <?= date_format(date_create($order["cas_narocila"]), "H:i") ?></h5>
                    <?php } ?>
                    <h5>Products: <?= count($order["artikli"]) ?></h5>
                    <h5>Total: <?= $order["cena"] ?> €</h5>
                </div>
            </div>
        </a>
    <?php } ?>
</div>

