<div class="my-3">
    <h2><?= $title ?></h2>
</div>

<?php if ($user["idVloga"] == 2) { ?>
    <div class="mb-3">
        <h5>Customer: <?= $order["narocnik"]["ime"] . " " . $order["narocnik"]["priimek"] ?></h5>
        <h5>Address: <?= $order["narocnik"]["naslov"] ?></h5>
        <h5>Date: <?= date_format(date_create($order["cas_narocila"]), "d. M. Y") ?></h5>
        <h5>Time: <?= date_format(date_create($order["cas_narocila"]), "H:i") ?></h5>
    </div>
    <h3>Products</h3>
    <hr>
<?php } ?>

<div class="list-group mb-3">
    <?php foreach ($order["artikli"] as $product) { ?>
        <div class="
             list-group-item 
             align-items-center">
            <div class="d-flex justify-content-between">
                <h4><?= $product["kolicina"] ?>x <?= $product["naziv"] ?></h4>
                <h5><?= $product["cena"] ?> €</h5>
            </div>
        </div>
    <?php } ?>
    <div class="
         list-group-item 
         align-items-center
         list-group-item-secondary">
        <div class="d-flex justify-content-between">
            <h4>Total:</h4>
            <h5><?= $order["cena"] ?> €</h5>
        </div>
    </div>
</div>
<div class="d-flex">
    <div class="mr-2">
        <a href="<?php echo base_url(); ?>orders" class="btn btn-warning">Back</a>
    </div>
    <?php if ($user["idVloga"] == 2) { ?>
        <?php echo form_open("orders/process_order") ?>
        <input type="hidden" value="<?= $order["idNakup"] ?>" name="id">
        <input type="submit" value="Process order" class="btn btn-success">
        </form>
    <?php } ?>
</div>

