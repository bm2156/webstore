<html>
    <head>
        <title><?= $title ?></title>
        <link rel="stylesheet" 
              href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" 
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" 
              crossorigin="anonymous">
        <script src="https://kit.fontawesome.com/5cff7ba9b2.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="<?php echo base_url(); ?>">WebStore</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link <?php
                        if (isset($path) && $path == 'home') {
                            echo 'active';
                        }
                        ?>" href="<?php echo base_url(); ?>">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php
                        if (isset($path) && $path == 'products') {
                            echo 'active';
                        }
                        ?>" href="<?php echo base_url(); ?>products">Products</a>
                    </li>
                    <?php if ($user["idVloga"] == 2) { ?>
                        <li class="nav-item">
                            <a class="nav-link <?php
                            if (isset($path) && $path == 'orders') {
                                echo 'active';
                            }
                            ?>" href="<?php echo base_url(); ?>orders">Orders</a>
                        </li>
                    <?php } ?>
                </ul>
                <?php if (!isset($user) || $user == NULL) { ?> 
                    <form class="form-inline my-2 my-lg-0">
                        <a class="btn btn-outline-primary my-2 my-sm-0 mr-2" href="<?php echo base_url(); ?>login">Login</a>
                        <a class="btn btn-outline-secondary my-2 my-sm-0" href="<?php echo base_url(); ?>register">Register</a>
                    </form>
                <?php } else { ?> 
                    <?php if ($user["idVloga"] == 3) { ?>
                        <div class="mr-3">
                            <a href="<?php echo base_url(); ?>cart" class="btn">
                                Cart <span class="badge badge-pill badge-primary"><?= $cart_size ?></span>
                            </a>
                        </div>
                    <?php } ?> 
                    <div class="nav-item dropdown">
                        <div class="dropdown-toggle" style="cursor: pointer" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php echo $user["ime"] . " " . $user["priimek"] ?>
                        </div>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="<?php echo base_url(); ?>profile">Profile</a>
                            <?php if ($user["idVloga"] == 1) { ?> 
                                <a class="dropdown-item" href="<?php echo base_url(); ?>users">Retailers</a>
                            <?php } else if ($user["idVloga"] == 2) { ?>
                                <a class="dropdown-item" href="<?php echo base_url(); ?>users">Customers</a>
                            <?php } else if ($user["idVloga"] == 3) { ?>
                                <a class="dropdown-item" href="<?php echo base_url(); ?>orders">Orders</a>
                            <?php } ?>
                            <div class="dropdown-divider"></div>
                            <form class="form-inline my-2 my-lg-0" action="<?php echo base_url(); ?>auth/logout_process">
                                <button class="dropdown-item">Log out</button>
                            </form>
                        </div>
                    </div>
                <?php } ?> 
            </div>
        </nav>

        <div class="container">