<div class="my-3 d-flex justify-content-between">
    <h2><?= $title ?></h2>
    <?php if (isset($user) && $user["idVloga"] == 2) { ?>
        <a href="<?php echo base_url(); ?>products/create" class="btn btn-success">Create</a>
    <?php } ?>
</div>

<div class="d-flex flex-wrap">
    <?php foreach ($products as $product) { ?>
        <div class="col-3">
            <div class="card">
                <div class="card-body">
                    <div class="">
                        <h4 class="card-title"><?= $product["naziv"] ?></h4>
                        <h6 class="card-title"><?= $product["cena"] ?> €</h6>
                    </div>

                    <?php if (isset($user) && $user["idVloga"] == 3) { ?>
                        <div class="float-right">
                            <?php echo form_open("users/add_to_cart") ?>
                            <input type="hidden" value="<?= $product["idArtikel"] ?>" name="id">
                            <input type="number" 
                                   min="<?php
                                   if (!isset($cart[$product["idArtikel"]])) {
                                       echo 1;
                                   } else {
                                       echo 0;
                                   }
                                   ?>" 
                                   value="<?php
                                   if (!isset($cart[$product["idArtikel"]])) {
                                       echo 1;
                                   } else {
                                       echo intval($cart[$product["idArtikel"]]);
                                   }
                                   ?>" name="amount" style="width: 50px;">
                            <input type="submit" value="<?php
                            if (!isset($cart[$product["idArtikel"]])) {
                                echo "Add to cart";
                            } else {
                                echo "Update cart";
                            }
                            ?>" class="btn btn-success">
                            </form>
                        </div>
                    <?php } else if (isset($user) && $user["idVloga"] == 2) { ?>
                        <a href="<?php echo base_url(); ?>products/<?= $product["idArtikel"] ?>" class="btn btn-success float-right">Edit</a>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>
</div>

