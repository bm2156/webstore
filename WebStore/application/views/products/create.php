<h2 class="my-3"><?= $title ?></h2>

<?php echo validation_errors() ?>

<?php echo form_open('products/create_process') ?>
<div style="width: 30%">
    <div class="input-group mb-3">
        <input required type="text" class="form-control" name="title" placeholder="Title">
    </div>
    <div class="input-group mb-3">
        <input required type="number" class="form-control" name="price" placeholder="Price" min="0" step="any">
    </div>
    <div>
        <a href="<?php echo base_url(); ?>products" class="btn btn-warning">Back</a>
        <input type="submit" class="btn btn-success mb-1" value="Create">
    </div>
</div>
</form>

