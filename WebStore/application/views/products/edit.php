<h2 class="my-3"><?= $title ?></h2>

<?php echo validation_errors() ?>

<?php echo form_open('products/edit_process') ?>
<div style="width: 30%">
    <input type="hidden" name="id" value="<?= $product["idArtikel"] ?>">
    <div class="input-group mb-3">
        <input required type="text" class="form-control" name="title" placeholder="Title" value="<?= $product["naziv"] ?>">
    </div>
    <div class="input-group mb-3">
        <input required type="number" class="form-control" name="price" placeholder="Price" min="0" step="any" value="<?= $product["cena"] ?>">
    </div>
    <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" name="active" id="customCheck1" <?= $product["aktiven"] == 1 ? 'checked="checked"' : NULL; ?>>
        <label class="custom-control-label" for="customCheck1">Active</label>
    </div>
    <div class="mt-2">
        <a href="<?php echo base_url(); ?>products" class="btn btn-warning">Nazaj</a>
        <input type="submit" class="btn btn-success" value="Shrani">
    </div>
</div>
</form>

