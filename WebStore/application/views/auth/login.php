<h2 class="my-3"><?= $title ?></h2>

<?php echo validation_errors() ?>

<?php echo form_open('auth/login_process') ?>
<div style="width: 30%">
    <div class="input-group mb-3">
        <input required type="text" class="form-control" name="email" placeholder="Email" aria-label="Username" aria-describedby="basic-addon1">
    </div>
    <div class="input-group mb-3">
        <input required type="password" class="form-control" name="password" placeholder="Password" aria-label="Username" aria-describedby="basic-addon1">
    </div>

    <div>
        <input type="submit" class="btn btn-success mb-1" value="Log In">
        <div>
            <a href="<?php echo base_url(); ?>register">Don't have an account yet?</a>
        </div>
    </div>
</div>
</form>