<?php

class User_model extends CI_Model {

    public function __construct() {
        parent::__construct();

        $this->load->database();
    }

    public function get($email = NULL, $active = NULL, $role = NULL) {
        $this->db->select('*');
        $this->db->from('Uporabnik u');
        $this->db->join('Vloga v', "u.idVloga = v.idVloga");

        if ($active != NULL) {
            $this->db->where("u.aktiven", $active);
        }

        if ($role != NULL) {
            $this->db->where("u.idVloga", $role);
        }

        if ($email == NULL) {
            return $this->db->get()->result_array();
        }

        $this->db->where('u.email', $email);
        $query = $this->db->get();

        $res = $query->result_array();
        if (count($res) == 0) {
            return null;
        } else {
            return $res[0];
        }
    }

    public function getById($id, $active = NULL, $role = NULL) {
        $this->db->select('*');
        $this->db->from('Uporabnik u');
        $this->db->join('Vloga v', "u.idVloga = v.idVloga");

        if ($active != NULL) {
            $this->db->where("u.aktiven", $active);
        }

        if ($role != NULL) {
            $this->db->where("u.idVloga", $role);
        }

        $this->db->where('u.idUporabnik', $id);
        $query = $this->db->get();

        $res = $query->result_array();
        if (count($res) == 0) {
            return NULL;
        } else {
            return $res[0];
        }
    }

    public function save($user) {
        $this->db->insert("Uporabnik", $user);
    }

    public function update($user) {
        $this->db->where('idUporabnik', intval($user["idUporabnik"]));
        $this->db->update('Uporabnik', $user);
    }

}
