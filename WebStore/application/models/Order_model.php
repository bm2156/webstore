<?php

class Order_model extends CI_Model {

    public function __construct() {
        parent::__construct();

        $this->load->database();
    }

    public function get($id = NULL, $idCustomer = NULL, $processed = NULL) {
        $this->db->select("*");
        $this->db->from("Nakup n");

        if (!is_null($processed)) {
            $this->db->where("n.zakljucen", $processed);
        }

        if ($idCustomer != NULL) {
            $this->db->where("n.idNarocnik", $idCustomer);
        }

        if ($id == NULL) {
            $res = $this->db->get()->result_array();

            foreach ($res as &$order) {
                $order['artikli'] = $this->getProductsForOrder($order["idNakup"]);
            }

            return $res;
        }

        $this->db->where('idNakup', $id);

        $res = $this->db->get()->result_array();

        if (count($res) == 0) {
            return null;
        } else {
            $res[0]['artikli'] = $this->getProductsForOrder($id);
            return $res[0];
        }
    }

    public function save($order) {
        $person = $order["person"];
        $products = $order["products"];

        $this->db->insert('Nakup', array(
            "zakljucen" => FALSE,
            "cas_narocila" => date_create()->format('Y-m-d H:i:s'),
            "idNarocnik" => $person["idUporabnik"],
            "obdelal" => NULL,
            "cas_obdelave" => NULL
        ));

        $orderId = $this->db->insert_id();

        foreach ($products as $productId => $amount) {
            $this->db->insert('Nakup_has_Artikel', array(
                "idNakup" => $orderId,
                "idArtikel" => $productId,
                "kolicina" => $amount
            ));
        }
    }

    public function update($processedOrder) {
        $this->db->where('idNakup', $processedOrder["idNakup"]);
        $this->db->update('Nakup', array(
            "zakljucen" => $processedOrder["zakljucen"],
            "obdelal" => $processedOrder["obdelal"],
            "cas_obdelave" => date_create()->format('Y-m-d H:i:s'),
        ));
    }

    public function getProductsForOrder($id) {
        $this->db->select("a.idArtikel, a.naziv, a.cena, nha.kolicina");
        $this->db->from("Nakup_has_Artikel nha");
        $this->db->join("Artikel a", "nha.idArtikel = a.idArtikel");
        $this->db->where("nha.idNakup", $id);

        return $this->db->get()->result_array();
    }

}
