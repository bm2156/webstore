<?php

class Product_model extends CI_Model {

    public function __construct() {
        parent::__construct();

        $this->load->database();
    }

    public function get($id = NULL, $active = NULL) {
        $this->db->select('*');
        $this->db->from('Artikel a');
        
        if($active != NULL) {
            $this->db->where('aktiven', $active);
        }

        if ($id == NULL) {
            return $this->db->get()->result_array();
        }

        $this->db->where('idArtikel', $id);

        $res = $this->db->get()->result_array();
        if (count($res) == 0) {
            return null;
        } else {
            return $res[0];
        }
    }

    public function save($product) {
        $this->db->insert("Artikel", $product);
    }

    public function update($product) {
        $this->db->where('idArtikel', $product["idArtikel"]);
        $this->db->update('Artikel', $product);
    }

}
