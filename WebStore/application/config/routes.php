<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$route['orders/process_order'] = 'orders/process_order';
$route['orders/process_order'] = 'orders/process_order';
$route['orders'] = 'orders/index';
$route['orders/(:any)'] = 'orders/view/$1';
$route['cart/checkout'] = 'cart/checkout';
$route['cart'] = 'cart/index';
$route['products/create'] = 'products/create';
$route['products/edit_process'] = 'products/edit_process';
$route['products/create_process'] = 'products/create_process';
$route['products'] = "products/index";
$route['products/(:any)'] = 'products/edit/$1';
$route['users/create'] = 'users/create';
$route['users/edit_process'] = 'users/edit_process';
$route['users/add_to_cart'] = 'users/add_to_cart';
$route['users/add_to_cart/(:any)'] = 'users/add_to_cart/$1';
$route['users'] = "users/index";
$route['users/(:any)'] = 'users/edit/$1';
$route['profile'] = 'profile/index';
$route['register'] = 'auth/register';
$route['login'] = 'auth/login';
$route['default_controller'] = 'pages/view';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
