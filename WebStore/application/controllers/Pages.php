<?php

class Pages extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('session');
    }

    public function view($page = 'home') {
        if (!file_exists(APPPATH . "views/pages/" . $page . ".php")) {
            show_404();
        }

        $data['title'] = ucfirst($page);
        $data['user'] = $this->session->user;
        $data['path'] = $page;

        if ($this->session->user["idVloga"] == 3) {
            $data['cart'] = $this->session->cart;
            $data['cart_size'] = count($this->session->cart);
        }

        $this->load->view("templates/head", $data);
        $this->load->view("pages/" . $page, $data);
        $this->load->view("templates/foot");
    }

}
