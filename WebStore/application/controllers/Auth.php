<?php

class Auth extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->load->library('session');
        $this->load->model('user_model');
    }

    public function login() {
        if (isset($this->session->user)) {
            redirect("/");
        }

        $data['title'] = 'Login';
        $data['user'] = $this->session->user;

        $this->load->view("templates/head", $data);
        $this->load->view("auth/login", $data);
        $this->load->view("templates/foot");
    }

    public function register() {
        if (isset($this->session->user)) {
            redirect("/");
        }

        $data['title'] = 'Register';

        $this->load->view("templates/head", $data);
        $this->load->view("auth/register", $data);
        $this->load->view("templates/foot");
    }

    public function login_process() {
        if (isset($this->session->user)) {
            redirect("/");
        }

        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_password');

        if ($this->form_validation->run() == FALSE) {
            $this->login();
        } else {
            $email = $this->input->post('email');
            $user = $this->user_model->get($email);
            unset($user["geslo"]);

            $this->session->user = $user;
            if ($user["idVloga"] == 3) {
                $this->session->cart = array();
            }

            $this->load->helper('url');
            redirect("/");
        }
    }

    public function register_process() {
        if (isset($this->session->user) && $this->session->user["idVloga"] == 3) {
            redirect("/");
        }

        $role = 3;
        if (isset($this->session->user) && $this->session->user["idVloga"] == 1) {
            $role = 2;
        } else {
            $this->form_validation->set_rules('phoneNumber', 'Phone number', 'trim|required|xss_clean');
            $this->form_validation->set_rules('address', 'Address', 'trim|required|xss_clean');
        }

        $this->form_validation->set_rules('firstname', 'First name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('lastname', 'Last name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            if (isset($this->session->userdata['logged_in'])) {
                $this->load->view('pages/home');
            } else {
                $data['title'] = 'Register';
                $this->load->view('auth/register', $data);
            }
        } else {
            $firstname = $this->input->post('firstname');
            $lastname = $this->input->post('lastname');
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $phoneNumber = $this->input->post('phoneNumber');
            $address = $this->input->post('address');

            $user = array(
                "ime" => $firstname,
                "priimek" => $lastname,
                "email" => $email,
                "geslo" => password_hash($password, PASSWORD_DEFAULT),
                "telefon" => $phoneNumber,
                "naslov" => $address,
                "aktiven" => TRUE,
                "idVloga" => $role
            );

            $this->user_model->save($user);

            if (!isset($this->session->user)) {
                redirect("/login");
            } else {
                redirect("/users");
            }
        }
    }

    public function logout_process() {
        session_destroy();
        redirect("/");
    }

    public function check_password() {
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $user = $this->user_model->get($email, TRUE);

        if ($user != null && password_verify($password, $user["geslo"]) == TRUE) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_password', 'Invalid username or password');
            return FALSE;
        }
    }

}
