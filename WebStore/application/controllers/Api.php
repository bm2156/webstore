<?php

use WebStore\Libraries\RestController;

require APPPATH . 'libraries/RestController.php';
require APPPATH . 'libraries/Format.php';

class API extends RestController {

    public function __construct() {
        parent::__construct();

        $this->load->model('product_model');
    }

    public function products_get() {
        $id = $this->get('id');

        if ($id != NULL) {
            $product = $this->product_model->get($id, TRUE);

            if (!is_null($product)) {
                $this->response($product, 200);
            } else {
                $this->response(array("message" => "Product not found"), 404);
            }
        } else {
            $this->response($this->product_model->get(NULL, TRUE), 200);
        }
    }

}
