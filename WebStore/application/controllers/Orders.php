<?php

class Orders extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('order_model');
        $this->load->model('user_model');
    }

    public function index() {
        $header['title'] = "Orders";
        $header['user'] = $this->session->user;
        $header['path'] = $this->uri->uri_string();


        $data['title'] = "Orders";
        $data['user'] = $this->session->user;

        if ($this->session->user['idVloga'] == 2) {
            $data['orders'] = $this->calculateTotal($this->order_model->get(NULL, NULL, FALSE));
        } else if ($this->session->user['idVloga'] == 3) {
            $header['cart'] = $this->session->cart;
            $header['cart_size'] = count($this->session->cart);

            $data['orders'] = $this->calculateTotal($this->order_model->get(NULL, $this->session->user["idUporabnik"]));
        } else {
            redirect('/products');
        }

        $this->load->view("templates/head", $header);
        $this->load->view("orders/list", $data);
        $this->load->view("templates/foot");
    }

    public function view($id) {
        $header['title'] = "Order";
        $header['user'] = $this->session->user;
        $header['path'] = $this->uri->uri_string();


        $data['title'] = "Order";
        $data['user'] = $this->session->user;

        if ($this->session->user['idVloga'] == 2) {
            $order = $this->order_model->get($id, NULL, FALSE);

            if ($order == NULL) {
                redirect("/orders");
            }

            $data['order'] = $this->calculateTotal(array($order))[0];
        } else if ($this->session->user['idVloga'] == 3) {
            $header['cart'] = $this->session->cart;
            $header['cart_size'] = count($this->session->cart);

            $order = $this->order_model->get($id, $this->session->user["idUporabnik"]);

            if ($order == NULL) {
                redirect("/orders");
            }

            $data['order'] = $this->calculateTotal(array($order))[0];
        } else {
            redirect('/products');
        }

        #var_dump($data["order"]);
        #exit();

        $this->load->view("templates/head", $header);
        $this->load->view("orders/view", $data);
        $this->load->view("templates/foot");
    }

    public function process_order() {
        if (!isset($this->session->user) || $this->session->user["idVloga"] != 2) {
            redirect("/orders");
        }

        $this->form_validation->set_rules('id', 'ID', 'trim|required|xss_clean');

        if ($this->form_validation->run() == TRUE) {
            $this->order_model->update(array(
                "idNakup" => $this->input->post("id"),
                "obdelal" => $this->session->user["idUporabnik"],
                "zakljucen" => TRUE
            ));

            redirect("/orders");
        }

        redirect("/orders");
    }

    private function calculateTotal($orders) {
        foreach ($orders as &$order) {
            $sum = 0;
            foreach ($order["artikli"] as $product) {
                $sum += $product["cena"] * $product["kolicina"];
            }

            $order["cena"] = $sum;

            $order["narocnik"] = $this->user_model->getById($order["idNarocnik"]);
        }


        return $orders;
    }

}
