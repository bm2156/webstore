<?php

class Cart extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->load->library('session');
        $this->load->model('product_model');
        $this->load->model('order_model');
    }

    public function index() {
        if (!isset($this->session->user) && $this->session->user["idVloga"] != 3) {
            redirect("/products");
        }

        $header['title'] = "Cart";
        $header['user'] = $this->session->user;
        $header['path'] = $this->uri->uri_string();
        $header['cart'] = $this->session->cart;
        $header['cart_size'] = count($this->session->cart);

        $data['title'] = "Cart";

        $data['items'] = array();
        $data['price_sum'] = 0;
        foreach ($this->session->cart as $productId => $amount) {
            $product = $this->product_model->get($productId);
            $data['price_sum'] += $product['cena'] * $amount;
            array_push($data['items'], array("product" => $product, "amount" => $amount));
        }

        $this->load->view('templates/head', $header);
        $this->load->view('cart/list', $data);
        $this->load->view('templates/foot');
    }

    public function checkout() {
        $header['title'] = "Products";
        $header['user'] = $this->session->user;
        $header['path'] = $this->uri->uri_string();

        $order["person"] = $this->session->user;
        $order["products"] = $this->session->cart;

        $this->order_model->save($order);

        $_SESSION["cart"] = array();

        $header['cart_size'] = 0;
        $header['cart'] = $this->session->cart;

        $this->load->view('templates/head', $header);
        $this->load->view('messages/order_success');
        $this->load->view('templates/foot');
    }

}
