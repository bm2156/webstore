<?php

class Profile extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('user_model');
    }

    public function index() {
        if (!isset($this->session->user)) {
            redirect("/login");
        }

        $header['title'] = "Profile";
        $header['user'] = $this->session->user;

        if (isset($this->session->cart)) {
            $header['cart'] = $this->session->cart;
            $header['cart_size'] = count($this->session->cart);
        }

        $this->load->view("templates/head", $header);
        $this->load->view("users/profile");
        $this->load->view("templates/foot");
    }

    public function update_details() {
        if (!isset($this->session->user)) {
            redirect("/login");
        }

        $this->form_validation->set_rules('firstname', 'First name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('lastname', 'Last name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
        $this->form_validation->set_rules('phoneNumber', 'Phone number', 'trim|xss_clean');
        $this->form_validation->set_rules('address', 'Address', 'trim|xss_clean');

        if ($this->form_validation->run() == TRUE) {
            $user = array(
                "idUporabnik" => intval($this->session->user["idUporabnik"]),
                "ime" => $this->input->post("firstname"),
                "priimek" => $this->input->post("lastname"),
                "telefon" => $this->input->post("phoneNumber"),
                "naslov" => $this->input->post("address"),
                "email" => $this->input->post("email"),
                "idVloga" => $this->session->user["idVloga"]
            );
            $this->user_model->update($user);

            $this->session->user = $this->user_model->get($user["email"]);
        }

        redirect("/profile");
    }

    public function change_password() {
        if (!isset($this->session->user)) {
            redirect("/login");
        }

        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_password');
        $this->form_validation->set_rules('changedPassword', 'Changed password', 'trim|required|xss_clean');

        if ($this->form_validation->run() == TRUE) {
            $password = $this->input->post("password");
            $changedPassword = $this->input->post("changedPassword");

            if (password_verify($password, $this->user_model->get($this->session->user["email"])["geslo"])) {
                $user = array(
                    "idUporabnik" => intval($this->session->user["idUporabnik"]),
                    "geslo" => password_hash($changedPassword, PASSWORD_DEFAULT)
                );

                $this->user_model->update($user);
                redirect("/profile");
            }
        }

        $this->index();
    }

    public function check_password() {
        $email = $this->session->user['email'];

        $password = $this->input->post("password");

        $user = $this->user_model->get($email);

        if (password_verify($password, $user["geslo"]) == TRUE) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_password', 'Invalid password');
            return FALSE;
        }
    }

}
