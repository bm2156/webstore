<?php

class Users extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('user_model');
    }

    public function index() {
        if (!isset($this->session->user) || $this->session->user["idVloga"] == 3) {
            redirect("/login");
        }

        if ($this->session->user["idVloga"] == 1) {
            $data["title"] = "Retailers";
            $header["title"] = "Retailers";
            $users = $this->user_model->get(NULL, NULL, 2);
        }

        if ($this->session->user["idVloga"] == 2) {
            $data["title"] = "Customers";
            $header["title"] = "Customers";
            $users = $this->user_model->get(NULL, NULL, 3);
        }

        $header["user"] = $this->session->user;
        $data["users"] = $users;

        $this->load->view("templates/head", $header);
        $this->load->view("users/list", $data);
        $this->load->view("templates/foot");
    }

    public function edit($id) {
        $user = $this->user_model->getById($id);
        if (!$this->can_edit($user["idVloga"])) {
            show_error("You cannot edit this user's details", 403, "Forbidden.");
        }

        unset($user["geslo"]);

        $data["user"] = $user;
        $data["title"] = "Edit";

        $header["title"] = "Edit | " . $user["ime"] . " " . $user["priimek"];
        $header["user"] = $this->session->user;

        $this->load->view("templates/head", $header);
        $this->load->view("users/edit", $data);
        $this->load->view("templates/foot");
    }

    public function create() {
        $role = "";
        if ($this->session->user["idVloga"] == 1) {
            $role = "retailer";
        } else if ($this->session->user["idVloga"] == 2) {
            $role = "customer";
        } else {
            show_error("You cannot create another user", 403, "Forbidden.");
        }

        $data["user"] = $this->session->user;
        $data["title"] = "Create " . $role;

        $header["title"] = "Create " . $role;
        $header["user"] = $this->session->user;

        $this->load->view("templates/head", $header);
        $this->load->view("users/create", $data);
        $this->load->view("templates/foot");
    }

    public function edit_process() {
        $this->form_validation->set_rules('id', 'ID', 'trim|required|xss_clean');
        $this->form_validation->set_rules('firstname', 'First name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('lastname', 'Last name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|xss_clean');
        $this->form_validation->set_rules('active', 'Active', 'trim|xss_clean');

        if (isset($this->session->user) && $this->session->user["idVloga"] == 2) {
            $this->form_validation->set_rules('phoneNumber', 'Phone number', 'trim|required|xss_clean');
            $this->form_validation->set_rules('address', 'Address', 'trim|required|xss_clean');
        }

        if ($this->form_validation->run() == TRUE) {
            $user = $this->user_model->getById($this->input->post('id'));

            if (!$this->can_edit($user["idVloga"])) {
                show_error("You cannot edit this user's details.", 403, "Forbidden");
            }

            $user["email"] = $this->input->post('email');
            $user["ime"] = $this->input->post('firstname');
            $user["priimek"] = $this->input->post('lastname');

            if ($this->input->post('active') != NULL) {
                $user["aktiven"] = TRUE;
            } else {
                $user["aktiven"] = FALSE;
            }

            if ($user["idVloga"] == 3) {
                $user["telefon"] = $this->input->post('phoneNumber');
                $user["naslov"] = $this->input->post('address');
            }

            if ($this->input->post('password') != NULL) {
                $user["geslo"] = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            }

            unset($user["vloga"]);

            $this->user_model->update($user);
            redirect("/users");
        } else {
            $this->edit($this->input->post('id'));
        }
    }

    public function can_edit($role) {
        $myRole = $this->session->user["idVloga"];

        if ($myRole == $role - 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function add_to_cart($redirect = "/products") {
        $this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean');
        $this->form_validation->set_rules('id', 'ID', 'trim|required|xss_clean');

        if ($this->form_validation->run() == TRUE) {
            if ($this->input->post('amount') == '0') {
                unset($_SESSION["cart"][$this->input->post('id')]);
            } else {
                $_SESSION["cart"][$this->input->post('id')] = $this->input->post('amount');
            }
            
            redirect($redirect);
        } else {
            redirect($redirect);
        }
    }

}
