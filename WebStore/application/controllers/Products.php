<?php

class Products extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->load->model("product_model");
    }

    public function index() {
        $header['title'] = "Products";
        $header['user'] = $this->session->user;
        $header['path'] = $this->uri->uri_string();

        if (isset($this->session->user) && $this->session->user["idVloga"] == 3) {
            $header['cart'] = $this->session->cart;
            $header['cart_size'] = count($this->session->cart);
        }


        $data['title'] = "Products";

        $active = TRUE;
        if ($this->session->user["idVloga"] == 2) {
            $active = NULL;
        }

        $data["products"] = $this->product_model->get(NULL, $active);

        $this->load->view('templates/head', $header);
        $this->load->view('products/list', $data);
        $this->load->view('templates/foot');
    }

    public function create() {
        if (!isset($this->session->user) || $this->session->user["idVloga"] != 2) {
            redirect("/products");
        }

        $header['title'] = "Create product";
        $header['user'] = $this->session->user;
        $data['title'] = "Create product";

        $this->load->view('templates/head', $header);
        $this->load->view('products/create', $data);
        $this->load->view('templates/foot');
    }

    public function edit($id) {
        $product = $this->product_model->get($id);

        $header['title'] = "Edit " . $product["naziv"];
        $header['user'] = $this->session->user;
        $data['title'] = "Edit " . $product["naziv"];
        $data['product'] = $product;

        $this->load->view('templates/head', $header);
        $this->load->view('products/edit', $data);
        $this->load->view('templates/foot');
    }

    public function create_process() {
        if (!isset($this->session->user) || $this->session->user["idVloga"] != 2) {
            redirect("/products");
        }

        $this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');
        $this->form_validation->set_rules('price', 'Price', 'trim|required|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $product = array(
                "naziv" => $this->input->post('title'),
                "cena" => $this->input->post('price'),
                "aktiven" => TRUE
            );

            $this->product_model->save($product);

            redirect("/products");
        }
    }

    public function edit_process() {
        if (!isset($this->session->user) || $this->session->user["idVloga"] != 2) {
            redirect("/products");
        }

        $this->form_validation->set_rules('id', 'ID', 'trim|required|xss_clean');
        $this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');
        $this->form_validation->set_rules('price', 'Price', 'trim|required|xss_clean');
        $this->form_validation->set_rules('active', 'Active', 'trim|xss_clean');

        $active = FALSE;

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            if ($this->input->post('active') != NULL) {
                $active = TRUE;
            }
            $product = array(
                "idArtikel" => $this->input->post('id'),
                "naziv" => $this->input->post('title'),
                "cena" => $this->input->post('price'),
                "aktiven" => $active
            );

            $this->product_model->update($product);

            redirect("/products");
        }
    }

}
